"""
Modulo donde se definen las clases que interactuaran con la bd postrges.
"""
from django.db import models

# Create your models here.
class User(models.Model):
    """
    Clase que crea la tabla usuarios en la bd workytest con los registros
    necesarios.
    """
    id = models.IntegerField(primary_key=True, auto_created=True)
    user = models.CharField(max_length = 20)
    password = models.CharField(max_length = 8)
    email = models.CharField(max_length = 50)
    first_name = models.CharField(max_length = 50)
    last_name = models.CharField(max_length = 50)

    def __str__(self):
        return '{}'.format(self.user)
