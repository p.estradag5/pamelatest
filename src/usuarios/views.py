"""
Modulo donde se controla el backend y regresa la respuesta en formato Json
"""
from django.views.generic import CreateView
from django.http import HttpResponse
import json

from .models import User


# Create your views here.
def usuario_list(request):
    """
    Funcion que consulta la base de datos y regresa todos los registros
    encontrados en ella en un formato Json.
    """
    users = User.objects.all()
    data = {'result': list(users.values('id', 'user', 'password', 'email',
                                        'first_name', 'last_name'))}
    data = json.dumps(data)
    return HttpResponse(data)

def get(request, idusuario):
    """
    Función que recibe un parámetro idusuario el cual es el id de algún usuario
    registrado en la base de datos y regresa el usuario registrado
    se llama: http://127.0.0.1:8000/list-users/get-user/<id>
    En caso de que el usuario no exista regresa un 204 Usuario no existe
    in:
     - idusuario: int
    return:
     - json
    """
    queryset = User.objects.filter(id=idusuario)
    if len(queryset) == 0:
        data = {'result': {
        'status': 204, 'message': "Usuario no existe en la base de datos"}}
    else:
        users = User.objects.get(id=idusuario)
        data = {
        'result': {
            'id': users.id,
            'user': users.user,
            'password': users.password,
            'email': users.email,
            'first_name': users.first_name,
            'last_name': users.last_name
            }
        }

    data = json.dumps(data)
    return HttpResponse(data)
