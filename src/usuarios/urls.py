"""
Módulo en la aplicación Usuarios donde se definen las urls que van
a interactuar para mostrar y/o pedir la información del usuario.
"""
from django.urls import path

from .views import usuario_list, get

urlpatterns = [
    #Lista todos los usuarios en la bd
    path('users-list/', usuario_list, name='usuario_list'),
    path('get-user/<idusuario>', get, name='get-user'),
]
